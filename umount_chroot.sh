#!/bin/bash

CHROOTDIR=$1

umount $CHROOTDIR/home/$SUDO_USER/.ssh
umount $CHROOTDIR/home/$SUDO_USER/shared
umount $CHROOTDIR/proc
umount $CHROOTDIR/sys
