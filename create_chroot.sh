#!/bin/bash

CHROOTDIR=$1
SINGLE_NAME=`basename $CHROOTDIR`

mkdir -p $CHROOTDIR

echo "
[xenial_$SINGLE_NAME]
description=Ubuntu_Xenial_$SINGLE_NAME
aliases=$SINGLE_NAME
directory=$CHROOTDIR
users=$SUDO_USER
groups=sbuild
root-groups=root
personality=linux
preserve-environment=true
" >> /etc/schroot/schroot.conf

debootstrap --variant=buildd --arch=amd64 xenial $CHROOTDIR http://archive.ubuntu.com/ubuntu/

echo "proc $CHROOTDIR/proc proc defaults 0 0" >> /etc/fstab
echo "sysfs $CHROOTDIR/sys sysfs defaults 0 0" >> /etc/fstab

cp /etc/hosts $CHROOTDIR/etc/hosts
cp /etc/passwd $CHROOTDIR/etc/passwd
cp /etc/shadow $CHROOTDIR/etc/shadow
cp /etc/group $CHROOTDIR/etc/group

mkdir -p $CHROOTDIR/home/$SUDO_USER/shared
mkdir -p $HOME/$SINGLE_NAME"_shared"
mkdir -p $CHROOTDIR/home/$SUDO_USER/.ssh

chown -R $SUDO_USER:$SUDO_USER $CHROOTDIR/
chown -R $SUDO_USER:$SUDO_USER $HOME/$SINGLE_NAME"_shared"/

echo "
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates main restricted
deb http://fr.archive.ubuntu.com/ubuntu/ xenial universe
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates universe
deb http://fr.archive.ubuntu.com/ubuntu/ xenial multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-updates multiverse
deb http://fr.archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu xenial-security main restricted
deb http://security.ubuntu.com/ubuntu xenial-security universe
deb http://security.ubuntu.com/ubuntu xenial-security multiverse
" >> $CHROOTDIR/etc/apt/sources.list



