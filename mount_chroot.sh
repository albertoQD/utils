#!/bin/bash

CHROOTDIR=$1
SCHROOT_SESSION=`basename $CHROOTDIR`

mount proc $CHROOTDIR/proc -t proc
mount sysfs $CHROOTDIR/sys -t sysfs
mount --bind $HOME/$SCHROOT_SESSION"_shared" $CHROOTDIR/home/$SUDO_USER/shared
mount --bind $HOME/.ssh/ $CHROOTDIR/home/$SUDO_USER/.ssh
schroot -u $SUDO_USER -c $CHROOTDIR
