# Utils scripts #

### Before using *_chroot.sh ###

Make sure to install `schroot` and `debootstrap` (you should have administration permit anyway)

Change the file `/etc/schroot/default/nssdatabases` and comment the lines for passwd, shadow and group

```
# System databases to copy into the chroot from the host system.
#
# <database name>
#passwd
#shadow
#group
gshadow
services
protocols
networks
hosts
```

### When using *_chroot.sh ###

Run all the scripts with `sudo` and the full path to the desire location of the chroot environment.

```
sudo ./create_chroot.sh /home/myuser/testing_env
sudo ./mount_chroot.sh /home/myuser/testing_env
...
$exit
sudo ./umount_chroot.sh /home/myuser/testing_env`
```

### For deleting an chroot environment ###

Delete the directory for the chroot environment (you'll have to sudo this)

`sudo rm -rf /home/myuser/testing_env`

Manually remove the added group on `/etc/schroot/schroot.conf`

Manually remove the added lines on `/etc/fstab`


Voilà!